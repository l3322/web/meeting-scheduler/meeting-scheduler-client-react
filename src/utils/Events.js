import { ReactSession } from 'react-client-session';

export const newMeeting = (response) => {
  let meetings = ReactSession.get('meetings')
  const newMeeting = {...response.meetingInfo, voted: false}
  if(meetings) {
    meetings.push(newMeeting)
  } else {
    meetings = [newMeeting]
  }
  ReactSession.set('meetings', meetings)
}

export const receivedVote = (response) => {
  let meetings = ReactSession.get('meetings')
  console.log(meetings)
  if(meetings) {
    for(var i = 0; i < meetings.length; i++) {
      if(meetings[i].name === response.meetingName) meetings[i].voted = true
    }
  }
  ReactSession.set('meetings', meetings)
}

export const endMeeting = (response) => {
  let meetings = ReactSession.get('meetings')
  if(meetings) {
    meetings = meetings.filter((meeting) => meeting.name !== response.meeting.meetingInfo.name)
  }
  ReactSession.set('meetings', meetings)
  let endedMeetings = ReactSession.get('endedMeetings')
  if(endedMeetings) {
    endedMeetings.push(response.meeting)
  } else {
    endedMeetings = [response.meeting]
  }
  ReactSession.set('endedMeetings', endedMeetings)
  console.log('Meeting ended: ' + response.meeting.meetingInfo.name)
  console.log(response.meeting)
}