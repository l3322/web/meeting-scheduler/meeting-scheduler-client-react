import React, { useEffect } from 'react';
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  Title
} from './NavigationElements';
import { ReactSession }  from 'react-client-session';
import { useState } from 'react';

const Navigation = ({ toggle }) => {
    const [isLogged, setIsLogged] = useState(false)

    useEffect(() => {
        const userLogged = ReactSession.get("userLogged")
        if(userLogged) {
            setIsLogged(true)
        }
    }, [])

    return (
        <>
            <Nav>
                <NavLink to='/'>
                    <Title>Meeting Scheduler App</Title>
                </NavLink>
                <Bars onClick={toggle} />
                <NavMenu>
                    <NavLink to='/'>
                        Meetings
                    </NavLink>
                    <NavLink to='/create'>
                        New meeting
                    </NavLink>
                </NavMenu>
            </Nav>
        </>
    );
};

export default Navigation;