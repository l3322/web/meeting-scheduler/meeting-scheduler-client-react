import React from 'react';
import {
  SidebarContainer,
  Icon,
  CloseIcon,
  SidebarWrapper,
  SidebarMenu,
  SidebarLink
} from './SidebarElements';
import { ReactSession }  from 'react-client-session';
import { useState, useEffect } from 'react';

const Sidebar = ({ isOpen, toggle }) => {
    const [isLogged, setIsLogged] = useState(false)

    useEffect(() => {
        const userLogged = ReactSession.get("userLogged")
        if(userLogged) {
            setIsLogged(true)
        }
    }, [])

    return (
        <SidebarContainer isOpen={isOpen} onClick={toggle}>
        {isLogged ?
            <>
                <Icon onClick={toggle}>
                    <CloseIcon />
                </Icon>
                <SidebarWrapper>
                    <SidebarMenu>
                    <SidebarLink
                        to='/meetings'
                        onClick={toggle}
                        smooth={true}
                        duration={500}
                        spy={true}
                        exact='true'
                    >
                        Meetings
                    </SidebarLink>
                    <SidebarLink
                        to='/create'
                        onClick={toggle}
                        smooth={true}
                        duration={500}
                        spy={true}
                        exact='true'
                    >
                        New meeting
                    </SidebarLink>
                    </SidebarMenu>
                </SidebarWrapper>
            </>
            : ''}
        </SidebarContainer>
    );
};

export default Sidebar;
