import React from 'react'
import { Card, Row, Col } from 'react-bootstrap';
import '../../styles.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Button } from 'react-bootstrap';

function CardGrid(props) {
    function onClick(e) {
        props.onClick(e.target.id)
    }
    return (
        <div className="flex justify-center">
            {props.cardList.length > 0 ?
                <div className="px-2 container">
                    <Row lg={6} className="g-2 py-3">
                        {props.cardList.map((card, idx) => (
                            <Col key={idx} > 
                                <Card className="grid-card">
                                    {card.hasBody ? 
                                        <Card.Body>
                                            <Card.Title>{card.title}</Card.Title>   
                                            <Card.Text>{card.text}</Card.Text> 
                                        </Card.Body>
                                    : card.text}
                                    {card.hasFooter ?
                                        <Card.Footer>
                                            <Button id={card.title} onClick={onClick}>{card.footer}</Button>
                                        </Card.Footer>
                                    : ''}
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </div>
            : ''}
        </div>
    )
}

export default CardGrid;