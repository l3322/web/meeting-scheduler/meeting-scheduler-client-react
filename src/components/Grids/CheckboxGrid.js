import React from 'react'
import { ToggleButtonGroup, ToggleButton , Row, Col } from 'react-bootstrap';
import '../../styles.css';
import 'bootstrap/dist/css/bootstrap.css';

function CheckboxGrid(props) {
    function onChecked(checkedList) {
        props.onClick(checkedList)
    }

    return (
        <div className="flex justify-center">
            {props.showGrid && props.checkboxList.length > 0 ?
                <div className="px-2 container">
                    <Row className="g-2 py-4">
                        {props.checkboxList.map((checkbox, idx) => (
                            <Col key={idx} >       
                                <ToggleButtonGroup
                                type='checkbox'
                                name='genre'
                                value={props.checkedList}
                                onChange={onChecked}>
                                    <ToggleButton   key={`toggle-${idx}`} 
                                                    id= {idx} 
                                                    value={idx} 
                                                    className={props.checkedList.includes(idx) ? 'btn-checked' : 'btn-unchecked'}> 
                                        {checkbox} 
                                    </ToggleButton>
                                </ToggleButtonGroup>
                            </Col>
                        ))}
                    </Row>
                </div>
            : ''}
        </div>
    )
}

export default CheckboxGrid;