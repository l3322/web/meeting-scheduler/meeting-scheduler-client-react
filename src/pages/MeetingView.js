import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom';
import { ReactSession }  from 'react-client-session';
import {Container, Card, Row, Col, Button} from 'react-bootstrap';
import CheckboxGrid from '../components/Grids/CheckboxGrid';
import { CallAPIRequestMeetingVote } from '../services/MeetingScheduler';
import CardGrid from '../components/Grids/CardGrid';

export default function MeetingView() {
  const location = useLocation()
  const [username, setUsername] = useState('')
  const [meetingName, setMeetingName] = useState('')
  const [nameCard, setNameCard] = useState({})
  const [ownerCard, setOwnerCard] = useState({})
  const [localCard, setLocalCard] = useState({})
  const [deadlineCard, setDeadlineCard] = useState({})
  const [timeCheckboxes, setTimeCheckboxes] = useState([])
  const [showTimes, setShowTimes] = useState(true)
  const [checkedList, setCheckedList] = useState([])
  const [showInfo, setShowInfo] = useState(false)
  const [voteCards, setVoteCards] = useState([])

  useEffect(() => {
    const username = ReactSession.get('username')
    setUsername(username)
    console.log(location.state)
    if(location.state) {
      const name = location.state.name
      setMeetingName(name)
      const meetings = ReactSession.get('meetings')
      let meetingInfo = {}
      if(meetings) {
        meetingInfo = meetings.filter((meeting) => meeting.name === name)[0]
      }
      setShowTimes(location.state.showTimes)
      setShowInfo(location.state.showInfo)
      console.log(location.state.showInfo)
      if(location.state.showInfo) {
        const endedMeetings = ReactSession.get('endedMeetings')
        console.log(endedMeetings)
        if(endedMeetings) {
          const meeting = endedMeetings.filter((meeting) => meeting.meetingInfo.name === name)[0]
          if(meeting){
            meetingInfo = meeting.meetingInfo
            const voteCards = buildVoteCards(meeting)
            setVoteCards(voteCards)
          }
        }
      }
      if(meetingInfo) {
        const cards = buildCards(meetingInfo)
        setNameCard(cards.name)
        setOwnerCard(cards.owner)
        setLocalCard(cards.local)
        setTimeCheckboxes(cards.timeCheckboxes)
        setDeadlineCard(cards.deadline)
      }
    }
    
  }, [location.state])

  function buildCards(meetingInfo) {
    let timeCheckboxes = []
    meetingInfo.availableTimes.forEach((time) =>{
      timeCheckboxes.push(time)
    })
    const cards = {
      name: {
        title: 'Name',
        hasBody: true,
        text: meetingInfo.name,
        hasFooter: false,
        footer: ''
      },
      owner: {
        title: 'Owner',
        hasBody: true,
        text: meetingInfo.owner,
        hasFooter: false,
        footer: ''
      },
      local: {
        title: 'Local',
        hasBody: true,
        text: meetingInfo.local,
        hasFooter: false,
        footer: ''
      },
      deadline: {
        title: 'Deadline',
        hasBody: true,
        text: meetingInfo.deadline,
        hasFooter: false,
        footer: ''
      },
      timeCheckboxes: timeCheckboxes
    }
    
    return cards
  }

  function buildVoteCards(meeting) {
    let voteCards = []
    for(var i = 0; i< meeting.dateVotes.value.length; i++) {
      const time = meeting.dateVotes.value[i][0]
      const vote = meeting.dateVotes.value[i][1]
      console.log(vote)
      voteCards.push({
        title: '',
        hasBody: true,
        text: time +" received " + vote.toString() + " votes",
        hasFooter: false,
        footer: ''
      })
    }
    return voteCards
  }

  function onTimeChecked(checkedList) {
    setCheckedList(checkedList)
  }

  function vote() {
    let dateList = []
    checkedList.forEach((idx) => {
      dateList.push(timeCheckboxes[idx])
    })
    CallAPIRequestMeetingVote(meetingName, username, dateList)
    .then((result) => {
      console.log(result)
    })
    .catch((err) => {
        console.log(err)
    })
  }

  return (
    <Container>
      <h1 className="page-header py-4">View Meeting</h1>
      <div className="px-2 container">
        <Row lg={3} className="g-2 py-3">
          <Col key={0} > 
            <Card className="grid-card">
              <Card.Body>
                <Card.Title>{nameCard.title}</Card.Title>   
                  <Card.Text>{nameCard.text}</Card.Text> 
              </Card.Body>
            </Card>
          </Col>
          <Col key={1} > 
            <Card className="grid-card">
              <Card.Body>
                <Card.Title>{ownerCard.title}</Card.Title>   
                  <Card.Text>{ownerCard.text}</Card.Text> 
              </Card.Body>
            </Card>
          </Col>
          <Col key={2} > 
            <Card className="grid-card">
              <Card.Body>
                <Card.Title>{localCard.title}</Card.Title>   
                  <Card.Text>{localCard.text}</Card.Text> 
              </Card.Body>
            </Card>
          </Col>
          <Col key={3} > 
            <Card className="grid-card">
              <Card.Body>
                <Card.Title>{deadlineCard.title}</Card.Title>   
                  <Card.Text>{deadlineCard.text}</Card.Text> 
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </div>
      {showTimes ?
        <>
          <h2 className="page-header py-4">Select your availability</h2>
          <CheckboxGrid showGrid={showTimes}  
                        onClick={onTimeChecked} 
                        checkboxList={timeCheckboxes}
                        checkedList={checkedList}/>
          <Button className="btn-default" onClick={() => vote()}>Vote</Button> 
        </>
      : <h2 className="page-header py-4">You have already voted</h2>}
      {showInfo ?
        <>
          <h2 className="page-header py-4">Result</h2>
          <CardGrid cardList={voteCards}/> 
        </>
      : ''}
      </Container>
  )
}
