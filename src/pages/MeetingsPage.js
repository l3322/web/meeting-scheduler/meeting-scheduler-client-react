import React from 'react'
import { ReactSession }  from 'react-client-session';
import { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import CardGrid from '../components/Grids/CardGrid';
import { useNavigate } from 'react-router-dom';

function MeetingsPage() {
  const [meetingList, setMeetingList] = useState([])
  const [endedMeetingList, setEndedMeetingList] = useState([])
  const navigate = useNavigate()

  useEffect(() => {
    const meetings = ReactSession.get("meetings")
    if(meetings) {
      const meetingList = buildMeetingsCards(meetings)
      setMeetingList(meetingList)
    }
    const endedMeetings = ReactSession.get("endedMeetings")
    if(endedMeetings) {
      const endedMeetingList = buildEndedMeetingsCards(endedMeetings)
      setEndedMeetingList(endedMeetingList)
    }
  }, [])

  function onView(name) {
    let meeting = meetingList.filter((meeting) => meeting.title === name)
    let showTimes = true
    let showInfo = false
    if(meeting.length > 0) {
      showTimes = !meeting[0].voted
      showInfo = meeting[0].hasEnded
    } else {
      showTimes = false
      showInfo = true
    }
    navigate('/view', {state:{name: name, showTimes: showTimes, showInfo: showInfo}})
  }

  function buildMeetingsCards(meetings) {
    const meetingList = []
    for(var i = 0; i < meetings.length; i++) {
      if(!meetings[i].hasEnded) {
        const meetingCard = {
            title: meetings[i].name,
            hasBody: true,
            text: 'Owner: ' + meetings[i].owner,
            hasFooter: true,
            footer: 'View',
            voted: meetings[i].voted,
            hasEnded: false
        }
        meetingList.push(meetingCard)
      }
    }
    return meetingList
  }

  function buildEndedMeetingsCards(meetings) {
    const meetingList = []
    for(var i = 0; i < meetings.length; i++) {
      const meetingCard = {
          title: meetings[i].meetingInfo.name,
          hasBody: true,
          text: 'Owner: ' + meetings[i].meetingInfo.owner,
          hasFooter: true,
          footer: 'View',
          hasEnded: true
      }
      meetingList.push(meetingCard)
    }
    return meetingList
  }

  return (
    <Container>
      <h1 className="page-header py-4">Meetings</h1>
      {meetingList.length > 0 ? 
        <>
          <CardGrid cardList={meetingList} onClick={onView}/>
        </>
      : <h3 className="page-header py-4">No meetings are currently available</h3>}
      {endedMeetingList.length > 0 ? 
        <>
          <h2 className="page-header py-4">Ended meetings</h2>}
          <CardGrid cardList={endedMeetingList} onClick={onView}/>
        </>
      : ''}
    
    </Container>
  )
}

export default MeetingsPage;