import {useEffect, useState} from 'react';
import Calendar from 'react-calendar'; 
import '../styles.css';
import Container from 'react-bootstrap/Container';
import CheckboxGrid from '../components/Grids/CheckboxGrid';
import moment from 'moment';
import CardGrid from '../components/Grids/CardGrid';
import { Button, Form } from 'react-bootstrap';
import { ReactSession } from 'react-client-session';
import { CallAPIRequestNewMeeting } from '../services/MeetingScheduler';
import {Row} from 'react-bootstrap';

function CreateEventPage() {
    const [date, setDate] = useState(new Date())
    const [showTime, setShowTime] = useState(false)
    const [checkedMap, setCheckedMap] = useState({})
    const [times, setTimes] = useState([])
    const [checkedList, setCheckedList] = useState([])
    const [dateTimeList, setDateTimeList] = useState([])
    const [meetingName, setMeetingName] = useState('')
    const [meetingLocal, setMeetingLocal] = useState('')
    const [username, setUsername] = useState('')
    const [meetingDeadline, setMeetingDeadline] = useState('')
    
    
    function onTimeChecked(checkedList) {
        setCheckedList(checkedList)
        const dateString = moment(date).format('MM-DD-YYYY')
        checkedMap[dateString] = checkedList
        setCheckedMap(checkedMap)
        const dateTimeList = buildTimeCards(checkedMap, times)
        setDateTimeList(dateTimeList)
    }

    function buildTimes() {
        let dayEnded = false
        let times = []
        let minutes = 0
        let hours = 0
        while(!dayEnded){
            const minutesString = minutes < 10 ? '0' + minutes.toString() : minutes.toString()
            const hoursString = hours.toString()
            times.push(hoursString + ':' + minutesString)
            if(minutes + 10 === 60) {
                minutes = 0
                hours++
            } else {
                minutes = minutes + 10
            }
            
            if(hours >= 24) dayEnded = true
        }
        return times
    }

    function buildCheckedList(checkedMap, date){
        let checkedList = []
        const dayString = moment(date).format('MM-DD-YYYY')
        if(dayString in checkedMap) {
            checkedList = checkedMap[dayString]
        }           
        return checkedList
    }

    function buildTimeCards(checkedMap, times) {
        let sortableList = []
        for(var date in checkedMap) {
            for(var i=0; i < checkedMap[date].length; i++) {
                const checkedIdx = checkedMap[date][i]
                let time = times[checkedIdx]
                sortableList.push(date+ " " + time)
            }
        }
        sortableList.sort()
        let dateTimeList = []
        for(i = 0; i < sortableList.length; i++) {
            const dateCard = {
                title: "",
                hasBody: false,
                text: sortableList[i],
                hasFooter: false,
                footer: ""
            }
            dateTimeList.push(dateCard)
        }
        return dateTimeList
    }

    function handleChangeDate(value) {
        setDate(value)
        const checkedList = buildCheckedList(checkedMap, value)
        setCheckedList(checkedList)
        const dateTimeList = buildTimeCards(checkedMap, times)
        setDateTimeList(dateTimeList)
    }

    const addNewMeeting = async () => {
        let promise = new Promise( (resolve, reject) => {
            CallAPIRequestNewMeeting(username, meetingName, meetingLocal, meetingDeadline, dateTimeList)
            resolve('');
        });
        promise.then(() => {
            console.log('Meeting added')
        })
    }

    useEffect(()=> {
        const times = buildTimes()
        setTimes(times)
        const username = ReactSession.get("username")
        setUsername(username)
    }, [])

    return (
        <Container>
            <h1 className="page-header py-4">New meeting</h1>
            <div className="calendar-container">
                <Calendar onChange={handleChangeDate} 
                            value={date} 
                            onClickDay={() => setShowTime(true)}/>
            </div>
            <div className="text-center">
                Selected date: {date.toDateString()}
            </div>
            <CheckboxGrid   showGrid={showTime}  
                            onClick={onTimeChecked} 
                            checkboxList={times}
                            checkedList={checkedList}/>
            {dateTimeList.length > 0 ? 
                <>
                    <main role="main" className="col-lg-12 mx-auto">
                        <div className="content mx-auto">
                            <Row className="g-4">
                                <Form.Control onChange={(e) => setMeetingName(e.target.value)} size="lg" required type="text" placeholder="Meeting name" />
                                <Form.Control onChange={(e) => setMeetingLocal(e.target.value)} size="lg" required type="text" placeholder="Meeting local" />
                                <Form.Control onChange={(e) => setMeetingDeadline(e.target.value)} size="lg" required type="text" placeholder="Meeting deadline" />
                                <Button className="btn-default" onClick={addNewMeeting}>Create</Button> 
                            </Row>
                        </div>
                    </main>
                    <h2 className='page-header py-4'>Selected dates</h2>
                    <CardGrid cardList={dateTimeList}/>
                </>
            : ''}
        </Container>
    )

}

export default CreateEventPage;