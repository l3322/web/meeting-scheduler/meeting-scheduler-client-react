import React, { useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap';

function Home(props) {
  const [name, setName] = useState('')

  function onEnter() {
    if(name !== '') props.onClick(name)
  }

  return (
    <Container>
      <h1 className="page-header py-4">Login</h1>
      <Form.Control onChange={(e) => setName(e.target.value)} size="lg" required type="text" placeholder="Name" />
      <div className="py-5">
        <Button className="btn-default" onClick={onEnter}>Enter</Button>
      </div>
    </Container>
  )
}

export default Home;
