import axios from "axios";
import { getRestAPIUrlData } from "./APIData";

const headers = {
  'Content-Type': 'application/json'
};

export async function CallAPIRequestNewMeeting(username, name, local, deadline, dateListObject) {
  const restAPIUrlData = getRestAPIUrlData()
  const url = restAPIUrlData.baseUrl + restAPIUrlData.newMeetingUrl
  
  const dateList = []
  for(var dateObject in dateListObject) {
    dateList.push(dateListObject[dateObject].text)
  }

  const body = {
    owner: username,
    name: name,
    local: local,
    deadline: deadline,
    availableTimes: dateList
  }
  try{
    const axiosCaller = axios.create();
    await axiosCaller.post(url, body, {headers})
    return true
  }
  catch(error) {
    console.log(error)
    return null
  }
}

export async function CallAPIRequestMeetingList() {
  const restAPIUrlData = getRestAPIUrlData()
  const url = restAPIUrlData.baseUrl + restAPIUrlData.getMeetingsUrl

  try{
    const axiosCaller = axios.create();
    let response = await axiosCaller.get(url, {}, {headers})

    return response.data
  }
  catch(error) {
    console.log(error)
    return null
  }
}

export async function CallAPIRequestMeetingVote(meetingName, username, dateList) {
  const restAPIUrlData = getRestAPIUrlData()
  const url = restAPIUrlData.baseUrl + restAPIUrlData.voteMeetingUrl

  try{
    const axiosCaller = axios.create();
    let response = await axiosCaller.post(url, {meetingName: meetingName, clientName: username, voteList: dateList}, {headers})

    return response.data
  }
  catch(error) {
    console.log(error)
    return null
  }
}