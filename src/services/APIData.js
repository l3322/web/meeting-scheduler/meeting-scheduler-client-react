const restAPIUrlData = {
  baseUrl: '',
  newMeetingUrl: 'newmeeting/',
  getMeetingsUrl: 'meetings',
  voteMeetingUrl: 'vote'
};

export function getRestAPIUrlData(){
  restAPIUrlData.baseUrl = "http://localhost:4650/";
  return restAPIUrlData;
}