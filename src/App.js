import './styles.css';
import { Route, Routes } from "react-router-dom";
import CreateEventPage from './pages/CreateEventPage';
import Navigation from './components/Navigation/Navigation';
import Sidebar from './components/Sidebar/Sidebar';
import { ReactSession } from 'react-client-session';
import MeetingsPage from './pages/MeetingsPage';
import { useEffect, useRef, useState } from 'react';
import Home from './pages/Home';
import { endMeeting, newMeeting, receivedVote } from './utils/Events';
import MeetingView from './pages/MeetingView';
import { useNavigate } from 'react-router-dom';
import { useTransition, animated } from 'react-spring';

function App() {
  ReactSession.setStoreType("localStorage");
  const [isOpen, setIsOpen] = useState(false);
  const [username, setUsername] = useState("");
  const color= "#25274D";
  const navigate = useRef(useNavigate())
  const [eventMessage, setEventMessage] = useState("")
  const [showEvent, setShowEvent] = useState(false)


  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const onLogin = (username) => {
    setUsername(username)
    ReactSession.set("userLogged", true)
    ReactSession.set("username", username)
  }

  const eventMessageTransition = useTransition(showEvent, {
    from: {x: -100, y: 800, opacity: 0},
    enter: {x: 0, y: 0, opacity: 1},
    leave: {x: 100, y: 800, opacity: 0},
    trail: 100
  }) 

  const displayEventMessageAnimation = async () => {
    await setShowEvent(true)
    setTimeout(() => setShowEvent(false), 5000)
  }   

  document.body.style.backgroundColor = color;

  useEffect(() => {
    if(username !== "")
    {
      console.log("Server initialized")
      const source = new EventSource(`http://localhost:4650/register?username=` + username);

      source.addEventListener('open', () => {
        console.log('SSE opened!');
      });

      source.addEventListener('message', (e) => {
        const data = JSON.parse(e.data)
        if(data.id === 0) {
          newMeeting(data)
          navigate.current('/')
          setEventMessage('Meeting was created succesfully')
        }
        if(data.id === 1) {
          endMeeting(data)
          setEventMessage('Meeting ' + data.meeting.meetingInfo.name + ' has ended')
        }
        if(data.id === 2) {
          receivedVote(data)
          navigate.current('/')
          setEventMessage('Votes received')
        }
        displayEventMessageAnimation()
      });

      source.addEventListener('error', (e) => {
        console.error('Error: ',  e);
      });

      return () => {
        source.close();
      };
    }
  }, [username])

  return (
    <div className= "App">
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navigation toggle={toggle}/>
      {eventMessageTransition((style, item) =>
        item ? <animated.div style={style} className="feedbackPopup">{eventMessage}</animated.div> : ''
      )}
      {username !== "" ? 
      <Routes>
        <Route exact path="/" element={
          <MeetingsPage />
        }> </Route>
        <Route exact path="/view" element={
          <MeetingView />
        }> </Route>
        <Route exact path="/create" element={
          <CreateEventPage />
        }> </Route>
      </Routes>
      : <Home onClick={onLogin}/>}
    </div>
  );
}

export default App;
